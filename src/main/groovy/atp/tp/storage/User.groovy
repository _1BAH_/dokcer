package atp.tp.storage

import atp.tp.dto.UserDto
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter

@Getter
@Setter
@NoArgsConstructor
class User {
    String name;
    long requests;
    long id;

    User(String name, long requests, long id) {
        this.name = name
        this.requests = requests
        this.id = id
    }

    User(UserDto dto) {
        this.name = dto.getName()
        this.requests = 1
    }

    @Override
    String toString() {
        return "User{id=${id}, name=${name}, requests=${requests}}"
    }
}
