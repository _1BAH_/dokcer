package atp.tp.storage

import groovy.xml.XmlUtil
import jakarta.annotation.PostConstruct
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class XmlStorage {
    Logger log = LoggerFactory.getLogger(XmlStorage.class)

    @Value('${database.path}')
    String path

    Node storage

    @PostConstruct
    void foo() {
        log.info("Data loaded from file {}", path)
        storage = new XmlParser().parse(path)
        log.info("Loaded {} elements", storage.children().size())
    }

    Optional<User> getUserById(long id) {
        def rawData = storage.find {node ->
            return node.attribute("id") as long == id
        }

        def name = rawData?.name?.text() as String
        def requests = rawData?.requests?.text() as Long

        return rawData == null ? Optional.empty() as Optional<User> : Optional.of(new User(name, requests, id))
    }

    void storeUser(User user) {
        def id = getLastId() + 1

        def newRow = new NodeBuilder().row('id': id) {
            name(user.name)
            requests(user.requests)
        }

        storage.append(newRow)
        flush()

        log.info("Added new user with id={}", id)


        user.setId(id)
    }

    void increaseRequestsCount(User user) {
        def id = user.id
        def rawData = storage.find {node ->
            return node.attribute("id") as long == id
        }

        user.requests++;
        rawData.requests[0].value = "${user.requests}"
        flush()
    }

    private long getLastId() {
        return storage.children().size() > 0 ? storage.children().last().attribute("id") as long : 0
    }

    private void flush() {
        println(storage)

        def writer = new FileWriter(path)
        XmlUtil.serialize(storage, writer)
    }
}
