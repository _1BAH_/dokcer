package atp.tp.controllers

import atp.tp.dto.UserDto
import atp.tp.storage.User
import atp.tp.storage.XmlStorage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@Controller
class UserController {
    @Autowired
    XmlStorage storage

    @GetMapping('/hit/{id}')
    ResponseEntity<User> hit(@PathVariable("id") long id) {
        def user = storage.getUserById(id)

        if (user != null) {
            storage.increaseRequestsCount(user.get())
        }

        return user
                .map {new ResponseEntity<>(it, HttpStatus.OK)}
                .orElseGet {new ResponseEntity<>(HttpStatus.NOT_FOUND)}
    }

    @PostMapping('/create')
    ResponseEntity<User> createUser(@RequestBody UserDto userDto) {
        def user = new User(userDto)
        storage.storeUser(user)
        return new ResponseEntity<>(user, HttpStatus.OK)
    }

}
