package atp.tp.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

@Controller
class EchoController {
    @GetMapping("/echo")
    @ResponseBody
    String echo(@RequestParam(name="name") String name) {
        return "Hello " + name
    }
}
