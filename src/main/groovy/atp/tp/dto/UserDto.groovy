package atp.tp.dto

import lombok.AllArgsConstructor
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
class UserDto {
    String name
}
